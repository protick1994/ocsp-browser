import pyshark
import os

def get_files_from_path(path, extension):
    retset = set()
    for f in os.listdir(path):
        if f.endswith(extension):
            retset.add(path + f) # Need to track path as well
    return retset

ALLOWED_MODES = [1, 2, 3, 4]
MODE_DESC = ["all allowed", "only ocsp disabled", "only ocsp stapling disabled", "both ocsp and ocsp stapling disabled"]

mode = 1
mother_path_for_storage = "{}".format(MODE_DESC[mode - 1].replace(" ", "_"))
path_for_pcap = mother_path_for_storage + "/pcap/"
path_for_time_stat = mother_path_for_storage + "/time_data/"
path_for_ssl_key = mother_path_for_storage + "/ssl_keys/"

pcap_files = set()
pcap_paths = ['../firefox/' + path_for_pcap]
for path in pcap_paths:
    pcap_files.update(get_files_from_path(path, ".pcap"))


for pcap in pcap_files:
    shark_cap = pyshark.FileCapture(pcap, override_prefs={'ssl.keylog_file': '../firefox/' + path_for_ssl_key + "ssl_key.log"}, display_filter='ocsp')
    while True:
        try:
            packet = shark_cap.next()
            a = 1
        except Exception as e:
            p = 1
