import json

from firefox.tools import get_top_websites
# import matpotlib.pyplot as plt
import numpy as np

top_websites = get_top_websites()

f = open('result.json')
d = json.load(f)
from collections import defaultdict
mom = defaultdict(lambda : list())

for w in top_websites:
    json_key = w + ".json"
    if json_key not in d:
        continue
    load_delta = d[json_key]['with_ocsp']['loadtime'] - d[json_key]['without_ocsp']['loadtime']
    processing_delta = d[json_key]['with_ocsp']['processing'] - d[json_key]['without_ocsp']['processing']
    ssl_delta = d[json_key]['with_ocsp']['ssl_connection'] - d[json_key]['without_ocsp']['ssl_connection']
    tcp_delta = d[json_key]['with_ocsp']['tcp_connection'] - d[json_key]['without_ocsp']['tcp_connection']

    mom['load_delta'].append({"url": w, "value": load_delta, "base": d[json_key]['without_ocsp']['loadtime']})
    mom['processing_delta'].append({"url": w, "value": processing_delta, "base": d[json_key]['without_ocsp']['processing']})
    mom['ssl_delta'].append({"url": w, "value": ssl_delta, "base": d[json_key]['without_ocsp']['ssl_connection']})
    mom['tcp_delta'].append({"url": w, "value": tcp_delta, "base": d[json_key]['without_ocsp']['tcp_connection']})

with open("metrices.json", "w") as ouf:
    json.dump(mom, fp=ouf, indent=2)

for key in mom:
    labels = []
    base = []
    res = []
    total = 0
    anomaly, val, perc = 0, 0, 0
    for e in mom[key]:
        if e['base'] == 0:
            continue
        if e['value'] <= 0:
            anomaly += 1
            continue
        val += e['value']
        perc += (e['value'] / e['base']) * 100
        total += 1


    print(key, total, anomaly, val/total)



    # x = np.arange(len(labels))  # the label locations
    # width = 0.35  # the width of the bars
    #
    # fig, ax = plt.subplots()
    # rects1 = ax.bar(x - width / 2, men_means, width, label='Men')
    # rects2 = ax.bar(x + width / 2, women_means, width, label='Women')
    #
    # # Add some text for labels, title and custom x-axis tick labels, etc.
    # ax.set_ylabel('Scores')
    # ax.set_title('Scores by group and gender')
    # ax.set_xticks(x)
    # ax.set_xticklabels(labels)
    # ax.legend()
    #
    # ax.bar_label(rects1, padding=3)
    # ax.bar_label(rects2, padding=3)
    #
    # fig.tight_layout()
    #
    # plt.show()



