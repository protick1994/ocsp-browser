from os import listdir
from os.path import isfile, join
import json


without_ocsp_path = "/Users/protick.bhowmick/PriyoRepos/Ocsp-Browser/firefox/only_ocsp_disabled/time_data"
without_ocsp = [f for f in listdir(without_ocsp_path) if isfile(join(without_ocsp_path, f))]

with_ocsp_path = "/Users/protick.bhowmick/PriyoRepos/Ocsp-Browser/firefox/all_allowed/time_data"
with_ocsp = [f for f in listdir(with_ocsp_path) if isfile(join(with_ocsp_path, f))]

# without_all_path = "/Users/protick.bhowmick/PriyoRepos/Ocsp-Browser/without_both/time_data"
# without_all = [f for f in listdir(without_all_path) if isfile(join(without_all_path, f))]

without_ocsp = set(without_ocsp)
with_ocsp = set(with_ocsp)
# without_all = set(without_all)

common_list = with_ocsp.intersection(without_ocsp)
# common_list = common_list.intersection(without_all)

from collections import defaultdict
mother_dict = defaultdict(lambda: defaultdict(lambda : dict()))

latency_data = defaultdict(lambda : dict())

for common_site in common_list:
    try:
        f = open(with_ocsp_path + "/" + common_site)
        ocsp_data = json.load(f)

        f = open(without_ocsp_path + "/" + common_site)
        without_ocsp_data = json.load(f)

        # f = open(without_all_path + "/" + common_site)
        # without_all_data = json.load(f)

        # mother_dict[common_site]['with_ocsp']['load_time_manual'] = ocsp_data['load_time_manual']
        # mother_dict[common_site]['with_ocsp']['loadtime'] = ocsp_data['loadEventStart'] - ocsp_data['navigationStart']
        # mother_dict[common_site]['with_ocsp']['processing'] = ocsp_data['domComplete'] - ocsp_data['domLoading']
        # mother_dict[common_site]['with_ocsp']['tcp_connection'] = ocsp_data['connectEnd'] - ocsp_data['connectStart']
        mother_dict[common_site]['with_ocsp']['ssl_connection'] = ocsp_data['requestStart'] - ocsp_data['secureConnectionStart']

        resource_set_1 = set()
        resource_dict_1 = {}

        for element in ocsp_data['resource_timing']:
            if element['secureConnectionStart'] != 0 and element['requestStart'] != 0:
                resource_set_1.add(element['name'])
                resource_dict_1[element['name']] = element['requestStart'] - element['secureConnectionStart']

        # mother_dict[common_site]['without_ocsp']['load_time_manual'] = without_ocsp_data['load_time_manual']
        # mother_dict[common_site]['without_ocsp']['loadtime'] = without_ocsp_data['loadEventStart'] - without_ocsp_data['navigationStart']
        # mother_dict[common_site]['without_ocsp']['processing'] = without_ocsp_data['domComplete'] - without_ocsp_data['domLoading']
        # mother_dict[common_site]['without_ocsp']['tcp_connection'] = without_ocsp_data['connectEnd'] - without_ocsp_data['connectStart']
        mother_dict[common_site]['without_ocsp']['ssl_connection'] = without_ocsp_data['requestStart'] - without_ocsp_data[
            'secureConnectionStart']

        resource_set_2 = set()
        resource_dict_2 = {}

        for element in without_ocsp_data['resource_timing']:
            if element['secureConnectionStart'] != 0 and element['requestStart'] != 0:
                resource_set_2.add(element['name'])
                resource_dict_2[element['name']] = element['requestStart'] - element['secureConnectionStart']

        common_resource = resource_set_1.intersection(resource_set_2)

        latency_data[common_site]['base_diff'] = mother_dict[common_site]['with_ocsp']['ssl_connection'] - mother_dict[common_site]['without_ocsp']['ssl_connection']
        latency_data[common_site]['pre'] = mother_dict[common_site]['without_ocsp']['ssl_connection']
        latency_data[common_site]['post'] = mother_dict[common_site]['with_ocsp']['ssl_connection']


        for resource in common_resource:
            latency_data[common_site][resource] = resource_dict_1[resource] - resource_dict_2[resource]



        # mother_dict[common_site]['without_all']['load_time_manual'] = without_all_data['load_time_manual']
        # mother_dict[common_site]['without_all']['loadtime'] = without_all_data['loadEventStart'] - without_all_data['navigationStart']
        # mother_dict[common_site]['without_all']['processing'] = without_all_data['domComplete'] - without_all_data['domLoading']
        # mother_dict[common_site]['without_all']['tcp_connection'] = without_all_data['connectEnd'] - without_all_data['connectStart']
        # mother_dict[common_site]['without_all']['ssl_connection'] = without_all_data['requestStart'] - without_all_data[
        #     'secureConnectionStart']

        # load_time_delta = ocsp_data['load_time_manual'] - without_ocsp_data['load_time_manual']
        # tcp_connection_delta = (ocsp_data['connectEnd'] - ocsp_data['connectStart']) - (
        #             without_ocsp_data['connectEnd'] - without_ocsp_data['connectStart'])
        #
        # ssl_connection_delta = (ocsp_data['requestStart'] - ocsp_data['secureConnectionStart']) - (
        #         without_ocsp_data['requestStart'] - without_ocsp_data['secureConnectionStart'])
        #
        # print(ssl_connection_delta)
        # if float(ssl_connection_delta) == 0:
        #     a = 1
        #
        # mother_dict[common_site] = {}
        # mother_dict[common_site]['load_time_delta'] = load_time_delta
        # mother_dict[common_site]['ssl_connection_delta'] = ssl_connection_delta
        # mother_dict[common_site]['tcp_connection_delta'] = tcp_connection_delta
    except Exception as e:
        print(e)

with open("result.json", "w") as ouf:
    json.dump(latency_data, fp=ouf, indent=2)