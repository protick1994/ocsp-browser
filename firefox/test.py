import os
import subprocess
import time
from os import listdir
from os.path import isfile, join

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait

from firefox.tools import get_top_websites

# This example requires Selenium WebDriver 3.13 or newer
counter = 1
import json

'''
Modes
1 -> all allowed
2 -> only OCSP disabled
3 -> only OCSP stapling disabled
4 -> both OCSP and OCSP stapling disabled
'''
ALLOWED_MODES = [1, 2, 3, 4]
MODE_DESC = ["all allowed", "only ocsp disabled", "only ocsp stapling disabled", "both ocsp and ocsp stapling disabled"]
CONSIDER_PREV_RUN = False


top_websites = get_top_websites()
#top_websites = top_websites[: 2]

# for mode in ALLOWED_MODES:
for mode in [2]:

    mother_path_for_storage = "{}".format(MODE_DESC[mode - 1].replace(" ", "_"))
    path_for_pcap = mother_path_for_storage + "/pcap/"
    path_for_time_stat = mother_path_for_storage + "/time_data/"
    path_for_ssl_key = mother_path_for_storage + "/ssl_keys/"

    if not CONSIDER_PREV_RUN:
        import shutil
        ban_sites = []
        shutil.rmtree(path_for_pcap, ignore_errors=True)
        shutil.rmtree(path_for_time_stat, ignore_errors=True)
        shutil.rmtree(path_for_ssl_key, ignore_errors=True)

    from pathlib import Path

    Path(path_for_pcap).mkdir(parents=True, exist_ok=True)
    Path(path_for_time_stat).mkdir(parents=True, exist_ok=True)
    Path(path_for_ssl_key).mkdir(parents=True, exist_ok=True)

    os.environ[
        "SSLKEYLOGFILE"] = "{}ssl_key.log".format(path_for_ssl_key)

    if CONSIDER_PREV_RUN:
        prev_path = path_for_time_stat
        onlyfiles = [f for f in listdir(prev_path) if isfile(join(prev_path, f))]
        ban_sites = [e[0: -5] for e in onlyfiles]

    for website in top_websites:
        if website in ban_sites:
            print("skipped {}".format(website))
            continue

        profile = webdriver.FirefoxProfile()
        if mode == 1:
            profile.set_preference("security.OCSP.enabled", 1)
            profile.set_preference("security.ssl.enable_ocsp_stapling", True)
            profile.set_preference("security.enable_ocsp_must_staple", True)
        elif mode == 2:
            profile.set_preference("security.OCSP.enabled", 0)
            profile.set_preference("security.ssl.enable_ocsp_stapling", True)
            profile.set_preference("security.enable_ocsp_must_staple", True)
        elif mode == 3:
            profile.set_preference("security.OCSP.enabled", 1)
            profile.set_preference("security.ssl.enable_ocsp_stapling", False)
            profile.set_preference("security.enable_ocsp_must_staple", False)
        elif mode == 4:
            profile.set_preference("security.OCSP.enabled", 0)
            profile.set_preference("security.ssl.enable_ocsp_stapling", False)
            profile.set_preference("security.enable_ocsp_must_staple", False)

        counter += 1

        print("loading {} with mode {}".format(website, MODE_DESC[mode - 1]))

        '''
        Modes
        1 -> All allowed
        2 -> only OCSP disabled
        3 -> only OCSP stapling disabled
        4 -> both OCSP and OCSP stapling disabled
        '''


        try:
            options = Options()
            options.headless = True
            driver = webdriver.Firefox(options=options, executable_path='/Users/protick.bhowmick/gecko/geckodriver', firefox_profile=profile)
            #wait = WebDriverWait(driver, 10)
            ws = "https://" + website
            print("Now starting tcpdump")
            p = subprocess.Popen(
                ["tcpdump", "-i", 'en0', "-w", '{}{}.pcap'.format(path_for_pcap, website)],
                stdout=subprocess.PIPE)
            time.sleep(.5)
            starting_time = time.monotonic()
            driver.get(ws)
            page_load_time = time.monotonic() - starting_time
            # https://developer.mozilla.org/en-US/docs/Web/Performance/Navigation_and_resource_timings
            # https://w3c.github.io/perf-timing-primer/
            g = driver.execute_script("return window.performance.timing")
            g['load_time_manual'] = page_load_time
            g['resource_timing'] = driver.execute_script("return window.performance.getEntriesByType('resource')")
            print("Now terminating tcpdump")
            p.terminate()
            #driver.close()
            driver.quit()

            with open("{}{}.json".format(path_for_time_stat, website), "w") as ouf:
                json.dump(g, fp=ouf, indent=2)

        except Exception as e:
            print("Exception in loading {} with mode {}: {}".format(website, MODE_DESC[mode - 1], e))
            if driver:
                #driver.close()
                driver.quit()



