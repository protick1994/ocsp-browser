from csv import reader

# hard reset firefox
def get_top_websites():
    with open('top500Domains.csv', 'r') as read_obj:
        csv_reader = reader(read_obj)
        ans = []
        for row in csv_reader:
            ans.append(row[1])
        return ans[1: ]

